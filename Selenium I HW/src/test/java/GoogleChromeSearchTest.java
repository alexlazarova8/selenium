import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoogleChromeSearchTest {
    @Test
    public void searchInGoogle(){
        System.setProperty("webdriver.chrome.driver",
                "C:\\Alex\\programming\\Telerik - QA\\Selenium\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();

        webDriver.get("https://www.google.com/");
        webDriver.findElement(By.id("L2AGLb")).click();
        webDriver.findElement(By.xpath("//input[@class='gLFyf gsfi']")).sendKeys("Telerik Academy Alpha" + "\n");
        String telerikLink = webDriver.findElement(By.xpath("//div[@class='v7W49e']//div[1]//div//div//div//a/h3")).getText();
        Assert.assertEquals("IT Career Start in 6 Months - Telerik Academy Alpha",telerikLink);

        webDriver.quit();
    }
}
